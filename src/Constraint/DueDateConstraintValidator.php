<?php

declare(strict_types=1);

namespace App\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DueDateConstraintValidator extends ConstraintValidator
{
    /**
     * @param mixed      $value      The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if ('' === $value) {
            return;
        }

        $now = new \DateTime();
        $dateTimeValue = \DateTime::createFromFormat('Y-m-d', $value);

        if (!$dateTimeValue instanceof \DateTime) {
            return;
        }

        $now->setTime(0, 0, 0, 0);
        $dateTimeValue->setTime(0, 0, 0, 0);

        if ($now > $dateTimeValue) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }
}
