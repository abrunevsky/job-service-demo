<?php

declare(strict_types=1);

namespace App\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CategoryExists extends Constraint
{
    public $message = 'Category is unknown.';
}
