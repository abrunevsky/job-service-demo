<?php

declare(strict_types=1);

namespace App\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DueDateConstraint extends Constraint
{
    public $message = 'Due date cannot be past.';
}
