<?php

namespace App\Repository;

use App\Entity\Job;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Job|null find($id, $lockMode = null, $lockVersion = null)
 * @method Job|null findOneBy(array $criteria, array $orderBy = null)
 * @method Job[]    findAll()
 * @method Job[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobRepository extends ServiceEntityRepository
{
    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Job::class);
    }

    /**
     * @param null $author
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createQueryBuilderWithCategoryForAuthor($author = null)
    {
        $qb = $this->createQueryBuilder('j')
            ->innerJoin('j.category', 'c')
            ->addSelect('c')
        ;

        if ($author) {
            $qb->andWhere('j.creator = :author')
                ->setParameter('author', $author);
        }

        return $qb;
    }

    /**
     * @param int           $id
     * @param User|int|null $author
     *
     * @return Job|null
     */
    public function getOneWithCategory(int $id, $author = null): ?Job
    {
        return $this->createQueryBuilderWithCategoryForAuthor($author)
            ->andWhere('j.id = :jobId')
            ->setParameter('jobId', $id)
            ->getQuery()
            ->setHint(Query::HINT_CACHE_ENABLED, true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, false)
            ->getOneOrNullResult()
            ;
    }

    /**
     * @param int           $limit
     * @param User|int|null $author
     *
     * @return Job[]|Collection
     */
    public function getAllWithCategories(int $limit = 100, $author = null): array
    {
        return $this->createQueryBuilderWithCategoryForAuthor($author)
            ->setMaxResults($limit)
            ->orderBy('j.dueDate', 'ASC')
            ->orderBy('j.createdAt', 'ASC')
            ->getQuery()
            ->setHint(Query::HINT_CACHE_ENABLED, true)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, false)
            ->getResult()
            ;
    }
}
