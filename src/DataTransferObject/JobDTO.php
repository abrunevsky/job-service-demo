<?php

declare(strict_types=1);

namespace App\DataTransferObject;

use App\Constraint\CategoryExists;
use App\Constraint\DueDateConstraint;
use Symfony\Component\Validator\Constraints as Assert;

final class JobDTO
{
    /**
     * @var int|null
     *
     * @Assert\NotBlank()
     * @CategoryExists()
     */
    private $category_id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=5,
     *     max=50,
     *     minMessage="Title is too short.",
     *     maxMessage="Title is too long."
     * )
     */
    private $title;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $city;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="|^\d{5}$|",
     *     match=true,
     *     message="Zip must be german."
     * )
     */
    private $zip;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Date(message="Invalid date format.")
     * @DueDateConstraint(message="Due date cannot be past.")
     */
    private $due_date;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->category_id = (int) ($attributes['category']['id'] ?? $attributes['category_id'] ?? 0);

        if (!$this->category_id) {
            $this->category_id = null;
        }

        $this->title = (string) ($attributes['title'] ?? '');
        $this->city = (string) ($attributes['city'] ?? '');
        $this->zip = (string) ($attributes['zip'] ?? '');
        $this->description = (string) ($attributes['description'] ?? '');
        $this->due_date = (string) ($attributes['due_date'] ?? '');
    }

    /**
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return $this->category_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getDueDate(): string
    {
        return $this->due_date;
    }
}
