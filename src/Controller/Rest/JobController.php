<?php

declare(strict_types=1);

namespace App\Controller\Rest;

use App\DataTransferObject\JobDTO;
use App\Service\JobManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use JMS\Serializer\Exception\ValidationFailedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;

class JobController extends FOSRestController
{
    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @param JobManager $jobManager
     */
    public function __construct(JobManager $jobManager)
    {
        $this->jobManager = $jobManager;
    }

    /**
     * @Rest\Get("/jobs")
     * @Rest\View(serializerGroups={"job"})
     *
     * @return View
     */
    public function getJobs(): View
    {
        $jobs = $this->jobManager->getAll();

        return $this->createView($jobs, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/jobs")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postJob(Request $request): View
    {
        $inputJob = new JobDTO($request->request->all());
        $job = $this->jobManager->add($inputJob);

        return $this->createView($job, Response::HTTP_CREATED);
    }

    /**
     * @Rest\Get("/jobs/{jobId}")
     * @Rest\View(serializerGroups={"job"})
     *
     * @param int $jobId
     *
     * @return View
     */
    public function getJob(int $jobId): View
    {
        $job = $this->jobManager->get($jobId);

        return $this->createView($job, Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/jobs/{jobId}")
     * @Rest\View(serializerGroups={"job"})
     *
     * @param int     $jobId
     * @param Request $request
     *
     * @return View
     */
    public function putJob(int $jobId, Request $request): View
    {
        $inputJob = new JobDTO($request->request->all());
        $job = $this->jobManager->update($jobId, $inputJob);

        return $this->createView($job, Response::HTTP_OK);
    }

    /**
     * @Rest\Delete("/jobs/{jobId}")
     *
     * @param int $jobId
     *
     * @return View
     */
    public function deleteJob(int $jobId): View
    {
        $this->jobManager->remove($jobId);

        return $this->createView([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param mixed $data
     * @param int   $httpCode
     *
     * @return View
     */
    private function createView($data, int $httpCode = Response::HTTP_OK): View
    {
        $viewData = [];

        if (!empty($data)) {
            if ($data instanceof ValidationFailedException) {
                $errors = [];

                foreach ($data->getConstraintViolationList() as $violation) {
                    /* @var ConstraintViolation $violation */
                    $errors[$violation->getPropertyPath()] = $violation->getMessage();
                }

                $viewData = [
                    'code' => Response::HTTP_BAD_REQUEST,
                    'message' => $data->getMessage(),
                    'errors' => $errors,
                ];
            } else {
                $viewData = [
                    'code' => $httpCode,
                    'data' => $data,
                ];
            }
        }

        return $this->view($viewData, $httpCode);
    }
}
