<?php

declare(strict_types=1);

namespace App\Service;

use App\DataTransferObject\JobDTO;
use App\Entity\Category;
use App\Entity\Job;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use JMS\Serializer\Exception\ValidationFailedException;
use Symfony\Component\HttpKernel\Exception as HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class JobManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var User
     */
    private $user;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface     $validator
     */
    public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;

        // @todo: emulate real authentication instead of this shirt
        $id = (int) \getenv('DEV_AUTH_USER_ID');

        if (!$id) {
            throw new HttpException\UnauthorizedHttpException(
                'Please, define DEV_AUTH_USER_ID parameter as integer in a an .env'
            );
        }

        $this->user = $this->entityManager
            ->getRepository(User::class)
            ->find($id);

        if (!$this->user) {
            throw new HttpException\AccessDeniedHttpException(
                'The requested user does not exist in the system.'
            );
        }
    }

    /**
     * @param int $jobId
     *
     * @return Job
     *
     * @throws EntityNotFoundException
     */
    public function get(int $jobId): Job
    {
        $job = $this->entityManager
            ->getRepository(Job::class)
            ->getOneWithCategory($jobId, $this->user);

        if (!$job) {
            throw new EntityNotFoundException(
                \sprintf('Job #%d does not exist!', $jobId)
            );
        }

        return $job;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->entityManager
            ->getRepository(Job::class)
            ->getAllWithCategories(100, $this->user);
    }

    /**
     * @param JobDTO   $inputJob
     * @param Job|null $job
     *
     * @return Job|ValidationFailedException
     */
    private function assembleJobEntity(JobDTO $inputJob, Job $job = null)
    {
        $errors = $this->validator->validate($inputJob);

        if (\count($errors)) {
            return new ValidationFailedException($errors);
        }

        $category = $this->entityManager
            ->getRepository(Category::class)
            ->find($inputJob->getCategoryId());
        $now = new \DateTime();

        if (!$job) {
            $job = (new Job())
                ->setCreatedAt($now)
                ->setCreator($this->user);
        }

        $job->setCategory($category)
            ->setTitle($inputJob->getTitle())
            ->setZip($inputJob->getZip())
            ->setCity($inputJob->getCity())
            ->setDescription($inputJob->getDescription())
            ->setDueDate(\DateTime::createFromFormat('Y-m-d', $inputJob->getDueDate()))
            ->setUpdatedAt($now);

        return $job;
    }

    /**
     * @param JobDTO $inputJob
     *
     * @return Job|ValidationFailedException
     */
    public function add(JobDTO $inputJob)
    {
        $job = $this->assembleJobEntity($inputJob);

        if ($job instanceof ValidationFailedException) {
            return $job;
        }

        $this->entityManager->persist($job);
        $this->entityManager->flush();
        $this->entityManager->clear();

        return $job;
    }

    /**
     * @param int    $jobId
     * @param JobDTO $inputJob
     *
     * @return Job|ValidationFailedException
     */
    public function update(int $jobId, JobDTO $inputJob)
    {
        $job = $this->assembleJobEntity($inputJob, $this->get($jobId));

        if ($job instanceof ValidationFailedException) {
            return $job;
        }

        $this->entityManager->flush();

        return $job;
    }

    /**
     * @param int $jobId
     */
    public function remove(int $jobId): void
    {
        $job = $this->get($jobId);

        $this->entityManager->remove($job);
        $this->entityManager->flush();
    }
}
