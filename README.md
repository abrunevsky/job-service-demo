Job Service Demo application.
=============================
## Installation

1. Download a codebase using from the [gitlab repocitory][gitlab].
2. Copy `.env.dist` file into `.env`.
3. Run docker composer to build and start all the related containers. (See `make docker-up`)
4. Run composer install. (See `make composer-install`)
5. Run composer install. (See `make composer-install`)
6. Migrate all prepared database migrations. (See `make app-migrate`)
7. Load fixtures for tables `category` and `user`. (See `make db-fixtures-load`)
8. Now you can open page [http://localhost:8080][local]

[gitlab]:https://gitlab.com/abrunevsky/job-service-demo
[local]:http://localhost:8080 

You can use command `make app-build` that should automatically go throw all the steps listed above.
(See file Makefile for other help commands)

## Implemented REST routs:
```
 -------------------------- -------- -------- ------ -----------------------------------
  Name                       Method   Scheme   Host   Path
 -------------------------- -------- -------- ------ -----------------------------------
  app_rest_job_getjobs       GET      ANY      ANY    /api/jobs
  app_rest_job_postjob       POST     ANY      ANY    /api/jobs
  app_rest_job_getjob        GET      ANY      ANY    /api/jobs/{jobId}
  app_rest_job_putjob        PUT      ANY      ANY    /api/jobs/{jobId}
  app_rest_job_deletejob     DELETE   ANY      ANY    /api/jobs/{jobId}
 -------------------------- -------- -------- ------ -----------------------------------
```

## POST/PUT data JSON structure examples:
```
{
	"category": {
		"id": 108140
	},
	"title": "Job Service Title",
	"zip": "76512",
	"city": "Berlin",
	"due_date": "2018-11-21"
}
```
or
```
{
	"category_id": 108140,
	"title": "Job Service Title",
	"zip": "76512",
	"city": "Berlin",
	"description": "Optional field.",
	"due_date": "2018-11-21"
}
```

_Notes_:
1. These ways to pass a category reference are equivalent:
    * `"category_id": <id>` 
    * `"category": {"id": <id>}`
2. Field `description` is optional.

## Expected output formats:

### 1. `GET /api/jobs`:
```
{
    "code": 200,
    "data": [
        {
            "id": 1,
            "category": {
                "id": 108140,
                "name": "Kellersanierung"
            },
            "title": "Job Service Title",
            "zip": "76512",
            "city": "Berlin",
            "description": "Optional field.",
            "due_date": "2018-11-21T17:04:08+00:00",
            "created_at": "2018-10-22T13:43:12+00:00",
            "updated_at": "2018-10-22T17:04:08+00:00"
        },
        ...
    ]
}
```

### 2. `GET /api/job/1` | `PUT /api/job/1` | `POST /api/job`: 
```
{
    "code": 200,
    "data": {
        "id": 1,
        "category": {
            "id": 108140,
            "name": "Kellersanierung"
        },
        "title": "Job Service Title",
        "zip": "76512",
        "city": "Berlin",
        "description": "Optional field.",
        "due_date": "2018-11-21T17:04:08+00:00",
        "created_at": "2018-10-22T13:43:12+00:00",
        "updated_at": "2018-10-22T17:04:08+00:00"
    }
}
```

## Project contains as unit as functional tests.
Use command `make test` to run all the tests.