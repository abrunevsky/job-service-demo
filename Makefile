help:
	@echo ""
	@echo "usage: make COMMAND"
	@echo ""
	@echo "Commands:"
	@echo "  app-build                  Build application after code deployment"
	@echo "  app-destroy                Destroy application and delete all custom data"
	@echo "  app-cache-clear            Clear application cache"
	@echo "  app-make-entity            Start interactive process of creating new entity"
	@echo "  app-make-migration         Create migration file based on just created entities and database structure"
	@echo "  app-migrate                Apply new migrations"
	@echo "  composer-install           Install all related packarges"
	@echo "  composer-update            Update all related packarges"
	@echo "  db-reset                   Reset database and re-apply migrations and fixtures"
	@echo "  db-fixtures-load           Load database fixtures"
	@echo "  docker-down                Stop and destroy all docker containers"
	@echo "  docker-exec cmd=<command>  Execute a bash command in php container"
	@echo "  docker-ps                  List of running containers"
	@echo "  docker-rebuild             Rebuild all docker containers"
	@echo "  docker-restart             Restart all docker containers"
	@echo "  docker-up                  Create and start project's docker containers"
	@echo "  console cmd=<command>      Execute app console command"
	@echo "  php-cs-fixer               Run php-cs-fixer util"
	@echo "  shell                      Run php container shell in an interactive mode"
	@echo "  test                       Run phpunit test cases"

app-build:
	@cp -nf ./.env.dist ./.env 2> /dev/null
	@make docker-up
	@make composer-install
	@make app-migrate
	@make db-fixtures-load
	@echo "Success! Web server is running here http://localhost:8080."

app-destroy: docker-down
	@rm -f ./.env
	@rm -fR ./.docker/data
	@rm -fR ./bin/.phpunit
	@rm -fR ./vendor/*
	@rm -fR ./var/*

docker-up:
	@docker-compose up -d

docker-down:
	@docker-compose down -v

docker-ps:
	@docker-compose ps

docker-restart:
	@make docker-down
	@make docker-up

docker-exec: docker-up
	@docker-compose exec php-fpm $(cmd)

shell: docker-up
	@docker-compose exec php-fpm bash

composer-install: docker-up
	@docker-compose exec php-fpm composer install

composer-update: docker-up
	@docker-compose exec php-fpm composer update

php-cs-fixer:
	@docker-compose exec php-fpm ./vendor/bin/php-cs-fixer fix -v

console:
	@docker-compose exec php-fpm php bin/console $(cmd)

db-fixtures-load:
	@docker-compose exec php-fpm php bin/console hautelook:fixtures:load -n

app-cache-clear:
	@docker-compose exec php-fpm php bin/console cache:clear

app-make-entity:
	@docker-compose exec php-fpm php bin/console make:entity

app-make-migration:
	@docker-compose exec php-fpm php bin/console make:migration

app-migrate:
	@docker-compose exec php-fpm php bin/console doctrine:migration:migrate -n

db-reset:
	@echo "Resetting database..."
	@docker-compose exec php-fpm php bin/console doctrine:database:drop --force -n
	@docker-compose exec php-fpm php bin/console doctrine:database:create -n
	@make app-migrate
	@echo "Migrations applied."
	@make db-fixtures-load
	@echo "Fixtures loaded."

test:
	@docker-compose exec php-fpm php bin/phpunit ./tests

.PHONY: docker-up docker-down docker-restart docker-ps docker-exec shell
.PHONY: app-build app-destroy db-fixtures-load db-reset
.PHONY: composer-install composer-update php-cs-fixer test
.PHONY: console app-make-entity app-make-migration app-migrate app-cache-clear