<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class JobControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    private $testJobData = [];

    protected function setUp()
    {
        parent::setUp();

        $this->testJobData = [
            'category' => ['id' => 108140],
            'title' => 'Test Job',
            'zip' => '10115',
            'city' => 'Berlin',
            'description' => 'Some description...',
            'due_date' => '2019-12-12',
        ];

        $this->client = static::createClient(['test_case' => 'ParamFetcher']);
    }

    /**
     * @return int
     */
    public function testPostJob(): int
    {
        $this->client->request('POST', '/api/jobs', $this->testJobData);

        $this->assertSame(201, $this->client->getResponse()->getStatusCode());
        $this->assertArraySubset(
            [
                'code' => 201,
                'data' => $this->testJobData,
            ],
            $this->getResponseContentAsArray()
        );

        return $this->getResponseContentAsArray()['data']['id'] ?? 0;
    }

    /**
     * @depends testPostJob
     *
     * @param int $jobId
     */
    public function testGetJob(int $jobId)
    {
        $this->testJobData['id'] = $jobId;

        $this->client->request('GET', '/api/jobs/'.$jobId);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertArraySubset(
            [
                'code' => 200,
                'data' => $this->testJobData,
            ],
            $this->getResponseContentAsArray()
        );
    }

    /**
     * @depends testPostJob
     *
     * @param int $jobId
     */
    public function testGetJobList(int $jobId)
    {
        $this->client->request('GET', '/api/jobs');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $responseData = $this->getResponseContentAsArray();

        $this->assertArraySubset(
            [
                'code' => 200,
                'data' => [],
            ],
            $responseData
        );
        $this->assertGreaterThan(0, \count($responseData['data']));

        unset($this->testJobData['due_date']);
        $storedJob = \array_filter(
            $responseData['data'],
            function ($item) use ($jobId) {
                return $item['id'] === $jobId;
            }
        );
        // asserts that posted item is in the output list
        $this->assertArraySubset($this->testJobData, \current($storedJob));
    }

    /**
     * @depends testPostJob
     *
     * @param int $jobId
     */
    public function testPutJob(int $jobId)
    {
        // change 3 fields and remove optional
        $this->testJobData['category_id'] = 802030;
        $this->testJobData['city'] = 'Hamburg';
        $this->testJobData['zip'] = '21521';
        unset($this->testJobData['category']);
        unset($this->testJobData['description']);

        $this->client->request('PUT', '/api/jobs/'.$jobId, $this->testJobData);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertArraySubset(
            [
                'code' => 200,
                'data' => [
                    'category' => ['id' => 802030],
                    'title' => 'Test Job',
                    'zip' => '21521',
                    'city' => 'Hamburg',
                    'due_date' => '2019-12-12',
                ],
            ],
            $this->getResponseContentAsArray()
        );
    }

    /**
     * @depends testPostJob
     *
     * @param int $jobId
     *
     * @return int
     */
    public function testDeleteJobList(int $jobId): int
    {
        $this->client->request('DELETE', '/api/jobs/'.$jobId);

        $this->assertSame(204, $this->client->getResponse()->getStatusCode());
        $this->assertEmpty($this->client->getResponse()->getContent());

        return $jobId;
    }

    /**
     * @depends testDeleteJobList
     *
     * @param int $jobId
     */
    public function testGetJobFail(int $jobId)
    {
        $this->client->request('DELETE', '/api/jobs/'.$jobId);

        $this->assertSame(404, $this->client->getResponse()->getStatusCode());
        $this->assertArraySubset(
            [
                'code' => 404,
                'message' => 'Job #'.$jobId.' does not exist!',
            ],
            $this->getResponseContentAsArray()
        );
    }

    /**
     * @return array
     */
    private function getResponseContentAsArray(): array
    {
        return \json_decode(
            $this->client->getResponse()->getContent(),
            true
        );
    }
}
