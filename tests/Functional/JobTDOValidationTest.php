<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\DataTransferObject\JobDTO;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class JobTDOValidationTest extends WebTestCase
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    protected function setUp()
    {
        parent::bootKernel();

        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    public function testEmptyJob()
    {
        $job = new JobDTO([]);

        $this->assertValidation(
            [
                'category_id' => 'This value should not be blank.',
                'title' => 'This value should not be blank.',
                'city' => 'This value should not be blank.',
                'zip' => 'This value should not be blank.',
                'due_date' => 'This value should not be blank.',
            ],
            $job
        );
    }

    public function testJobSuccess()
    {
        $job = new JobDTO([
            'category' => ['id' => 411070],
            'title' => 'Test job',
            'city' => 'Berlin',
            'zip' => 10115,
            'due_date' => (new \DateTime('+1 day'))->format('Y-m-d'),
            'description' => 'test job description',
        ]);

        $this->assertValidation([], $job);
    }

    public function testJobTitleTooShort()
    {
        $job = new JobDTO([
            'category_id' => 411070,
            'title' => 'H1',
            'city' => 'Berlin',
            'zip' => 10115,
            'due_date' => (new \DateTime('+1 day'))->format('Y-m-d'),
            'description' => 'test job description',
        ]);

        $this->assertValidation(['title' => 'Title is too short.'], $job);
    }

    public function testJobTitleTooLong()
    {
        $job = new JobDTO([
            'category_id' => 411070,
            'title' => "That's a very-very-very-very-very-very-very long title",
            'city' => 'Berlin',
            'zip' => 10115,
            'due_date' => (new \DateTime('+1 day'))->format('Y-m-d'),
            'description' => 'test job description',
        ]);

        $this->assertValidation(['title' => 'Title is too long.'], $job);
    }

    public function testJobDateInPast()
    {
        $job = new JobDTO([
            'category_id' => 411070,
            'title' => "That's a title",
            'city' => 'Berlin',
            'zip' => 10115,
            'due_date' => '2001-01-01',
        ]);

        $this->assertValidation(['due_date' => 'Due date cannot be past.'], $job);
    }

    public function testJobIncorrectDate()
    {
        $job = new JobDTO([
            'category_id' => 411070,
            'title' => "That's a title",
            'city' => 'Berlin',
            'zip' => '10115',
            'due_date' => 'XXII century',
        ]);

        $this->assertValidation(['due_date' => 'Invalid date format.'], $job);
    }

    public function testJobZipIncorrect()
    {
        $job = new JobDTO([
            'category' => ['id' => 411070],
            'title' => 'Test job',
            'city' => 'Berlin',
            'zip' => '10115-17',
            'due_date' => (new \DateTime('+1 day'))->format('Y-m-d'),
            'description' => 'test job description',
        ]);

        $this->assertValidation(['zip' => 'Zip must be german.'], $job);
    }

    /**
     * @param array  $expectedErrors
     * @param JobDTO $job
     */
    protected function assertValidation(array $expectedErrors, JobDTO $job)
    {
        $errors = $this->validator->validate($job);
        $formattedErrors = self::violationListAsArray($errors);

        $this->assertEquals(
            $expectedErrors,
            $formattedErrors,
            'Actual violations are not equal to the expected'
        );
    }

    /**
     * @param ConstraintViolationList $violations
     *
     * @return array
     */
    private static function violationListAsArray(ConstraintViolationList $violations): array
    {
        $errors = [];

        foreach ($violations as $violation) {
            /* @var ConstraintViolation $violation */
            $errors[$violation->getPropertyPath()] = $violation->getMessage();
        }

        return $errors;
    }
}
