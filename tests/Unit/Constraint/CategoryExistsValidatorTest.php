<?php

declare(strict_types=1);

namespace App\Tests\Unit\Constraint;

use App\Entity\Category;
use App\Constraint\CategoryExists;
use App\Constraint\CategoryExistsValidator;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class CategoryExistsValidatorTest extends ConstraintValidatorTestCase
{
    /**
     * @return array
     */
    private function getCategoryFixtureMap(): array
    {
        return [
            10101 => (new Category())->setName('SomeJobService'),
            20202 => (new Category())->setName('AnotherJobService'),
        ];
    }

    protected function createValidator()
    {
        $categoryRepository = $this->createMock(ObjectRepository::class);
        $categoryRepository->expects($this->once())
            ->method('find')
            ->willReturnCallback(function (int $categoryId) {
                $categoriesMap = $this->getCategoryFixtureMap();

                return \array_key_exists($categoryId, $categoriesMap)
                    ? $categoriesMap[$categoryId]
                    : null;
            });

        $objectManager = $this->createMock(ObjectManager::class);
        $objectManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($categoryRepository);

        return new CategoryExistsValidator($objectManager);
    }

    public function testCategoryExists()
    {
        $this->validator->validate(10101, new CategoryExists());

        $this->assertNoViolation();
    }

    public function testCategoryDoesNotExist()
    {
        $constraint = new CategoryExists([
            'message' => 'UnknownCategory',
        ]);

        $this->validator->validate(50505, $constraint);

        $this->buildViolation('UnknownCategory')
            ->setParameter('{{ value }}', 50505)
            ->assertRaised();
    }
}
