<?php

declare(strict_types=1);

namespace App\Tests\Unit\Constraint;

use App\Constraint\DueDateConstraint;
use App\Constraint\DueDateConstraintValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class DueDateConstraintValidatorTest extends ConstraintValidatorTestCase
{
    protected function createValidator()
    {
        return new DueDateConstraintValidator();
    }

    public function testDueDateToday()
    {
        $now = new \DateTime();
        $this->validator->validate($now->format('Y-m-d'), new DueDateConstraint());

        $this->assertNoViolation();
    }

    public function testDueDateFuture()
    {
        $this->validator->validate('3018-10-22', new DueDateConstraint());

        $this->assertNoViolation();
    }

    public function testDueDatePast()
    {
        $constraint = new DueDateConstraint([
            'message' => 'myConstraint',
        ]);

        $this->validator->validate('1961-10-22', $constraint);

        $this->buildViolation('myConstraint')
            ->setParameter('{{ value }}', '1961-10-22')
            ->assertRaised();
    }
}
